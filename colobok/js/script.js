const PacMan = document.querySelector(".circle");
let step = 25;
const array = ["speed_1 = 25 m/s (shift + 1)",
    "speed_2 = 50 m/s (shift + 2)",
    "speed_3 = 100 m/s (shift + 3)"];
const panel = document.createElement('div');
panel.style.cssText = `position: absolute; right: 15px; width:200px; color: black; size:16px`;
array.forEach(function (elem) {
    let li = document.createElement('p');
    li.innerHTML = elem;
    li.id = "speed" + elem.slice( 6, 7);
    let result = panel.append(li);
});
document.body.append(panel);
document.getElementById("speed1").style.backgroundColor = "lightblue";


document.addEventListener('keydown', (event) => {
    if (event.shiftKey && event.code === "Digit3") {
        document.getElementById("speed3").style.backgroundColor = "lightblue";
        document.getElementById("speed2").style.backgroundColor = null;
        document.getElementById("speed1").style.backgroundColor = null;
        return step = 100;
     }
});
document.addEventListener('keydown', (event) => {
    if (event.shiftKey && event.code === "Digit2") {
        document.getElementById("speed2").style.backgroundColor = "lightblue";
        document.getElementById("speed3").style.backgroundColor = null;
        document.getElementById("speed1").style.backgroundColor = null;
        return step = 50;}
});
document.addEventListener('keydown', (event) => {
    if (event.shiftKey && event.code === "Digit1") {
        document.getElementById("speed1").style.backgroundColor = "lightblue";
        document.getElementById("speed2").style.backgroundColor = null;
        document.getElementById("speed3").style.backgroundColor = null;
        return step = 25;}
});

document.addEventListener('keydown', (event) => {
    console.log(step);
    let coords = PacMan.getBoundingClientRect();

    switch (event.key) {
        case "ArrowUp": {
            if (coords.top >= (step)) { PacMan.style.top = coords.top - step + "px";
                PacMan.style.transform = "rotate(-90deg)";}
            else {PacMan.style.top = 0 + "px";
                PacMan.style.transform = "rotate(-90deg)";}
            break;}

        case "ArrowDown": {
            if (coords.top <= (500 - step)) {PacMan.style.top = coords.top + step + "px";
                PacMan.style.transform = "rotate(90deg)";}
            else {PacMan.style.top = 500 + "px";
                PacMan.style.transform = "rotate(90deg)";}
            break;}

        case "ArrowLeft": {
            if (coords.left >= (step)) {PacMan.style.left = coords.left - step + "px";
                PacMan.style.transform = "rotate(180deg)";}
            else {PacMan.style.left = 0 + "px";
                PacMan.style.transform = "rotate(180deg)";}
            break;
        }
        case "ArrowRight": {
            if (coords.left <= (screen.width - step - 203)) {PacMan.style.left = coords.left + step + "px";
                PacMan.style.transform = "rotate(0deg)";}
            else {PacMan.style.left = screen.width - 203 + "px";
                PacMan.style.transform = "rotate(0deg)";}
            break;
        }
    }
});

// * Реализовать несколько режимов шага пэкмэна, показывать эту справку пользователю всегда в правом верхнем углу и подсвечивать синим цветом активный режим:
//     *   1 - одно нажатие = сдвиг на 25рх. Включается с помощью сочетания клавиш Ctrl+1
// *   2 - одно нажатие = сдвиг на  50рх Включается с помощью сочетания клавиш Ctrl+2
// *   3 - одно нажатие = сдвиг на 100px Включается с помощью сочетания клавиш Ctrl+3
// * Разворачивать пэкмэна в зависимости от направления движения.
// * Проверять не вышел ли пэкмэн за границы поля.