//запит даних
const numberOne = +prompt("Ведіть перше число", 1);
const numberTwo = +prompt("Ведіть друге число", 1);
const mathAction = prompt("Оберіть математичну дію: +, -, *, /", "+");
//вирішення та вивід відпові
function mathTask(firstValue,secondValue, sign) {
    switch (sign) {
        case "+":
            console.log(firstValue + secondValue);
            break;
        case "-":
            console.log(firstValue - secondValue);
            break;
        case "*":
            console.log(firstValue * secondValue);
            break;
        case "/":
            console.log(firstValue / secondValue);
            break;
    }
}
mathTask(numberOne, numberTwo, mathAction);