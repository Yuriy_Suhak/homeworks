function makeList(ourArray) {
    let ul = document.createElement('ul');
    ul.innerHTML = ourArray.map(item => `<li>little ${item}</li>`).join("and");
    document.querySelector("script").before(ul);
}
makeList(["frog", "rabbit", "fox", "bee", "beaver", "mouse", "crazy  cat"]);