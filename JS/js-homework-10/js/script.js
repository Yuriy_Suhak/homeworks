const redAlertText = document.createElement("p");
confirmBtn.after(redAlertText);
redAlertText.hiddenn = true;

firstInput.addEventListener('click', () => {
    if (firstInput.className.includes("slash")) {
        firstInput.classList.remove("fa-eye-slash");
        firstInput.previousElementSibling.type = "password";
    }
    else {
        firstInput.classList.add("fa-eye-slash");
        firstInput.previousElementSibling.type = "text";
    }
});

secondInput.addEventListener('click', () => {
    if (secondInput.className.includes("slash")) {
        secondInput.classList.remove("fa-eye-slash");
        secondInput.previousElementSibling.type = "password";
    }
    else {
        secondInput.classList.add("fa-eye-slash");
        secondInput.previousElementSibling.type = "text";
    }
});

confirmBtn.addEventListener('click', () => {
    if(firstInput.previousElementSibling.value === secondInput.previousElementSibling.value && firstInput.previousElementSibling.value + secondInput.previousElementSibling.value !== "") {
        redAlertText.hidden = true;
        alert("Вітаю!");
            }
    else if (firstInput.previousElementSibling.value + secondInput.previousElementSibling.value === "") {
        redAlertText.innerText = "Введіть хоча б щось";
        redAlertText.hidden = false;
        }
    else {
        redAlertText.innerText = "Паролі мають збігатися";
        redAlertText.hidden = false;
    }
    return false;
});
