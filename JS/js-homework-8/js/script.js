const inputPrice = document.forms['inputForm'];
const inputArea = document.getElementsByName('price')[0];
const span = document.createElement("span");
const button = document.createElement("img");
const redAlertText = document.createElement("p");
redAlertText.innerText = "Please enter correct price";
inputPrice.classList.add("input-box");

inputPrice.addEventListener("focus", () => inputPrice.classList.add('in-green'), true);
inputPrice.addEventListener("blur", () => {
    if(inputArea.value >= 0) {
        creatSpan();
    }
    else {
        redAlert();
        if(document.body.querySelector("span")) {
            clear();
        }

    }
   }, true);

function creatSpan() {
    inputPrice.classList.remove('in-green');
    inputPrice.classList.remove('in-red');
    span.innerText = `Поточна ціна: ${inputArea.value}`;
    span.classList.add('top-span');
    button.src = "img/close.png";
    button.classList.add('close');
    document.body.appendChild(span);
    document.body.appendChild(button);
    inputArea.classList.add('input-green');
    if(document.body.querySelector("p")){
        document.body.removeChild(redAlertText);
    }

}

button.onclick = function() {
    clear();
};

    function clear() {
    inputArea.value = null;
    inputArea.classList.remove('input-green');
    inputPrice.classList.remove('in-green');
    document.body.removeChild(span);
    document.body.removeChild(button);
   }

function redAlert() {
    document.body.appendChild(redAlertText);
    inputPrice.classList.remove('in-green');
    inputPrice.classList.add('in-red');
}