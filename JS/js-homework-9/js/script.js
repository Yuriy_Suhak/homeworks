for (let i = 1; i < article.children.length; ++i) {
    article.children[i].hidden = true
}

navBar.addEventListener('click', (event) => {
    for (let i = 0; i < navBar.children.length; ++i) {
        navBar.children[i].classList.remove("tabs-title-active")
    }
    let tabName = event.target.innerText;
    event.target.classList.add("tabs-title-active");
    for (let i = 0; i < article.children.length; ++i) {
        article.children[i].hidden = true
    }
    for (let r = 0; r < (article.childNodes.length -2) ; ++r) {
        const comm = article.childNodes[r].textContent;
        if(comm.includes(tabName)) {
            if(article.childNodes[r+2].hidden){
                article.childNodes[r+2].hidden = false;
            }
        }
            }
});