//key Up
$(document).scroll(function (e) {
    const $screenHeight = $(window).innerHeight();
    const $screenTop = $(window).scrollTop();

    if($screenTop > $screenHeight){
        if(!$('.scroll-top-btn').length) {
            const $scrollTopBtn = $('<button class="scroll-top-btn"></button>');
            $('script:first').before($scrollTopBtn);
            $scrollTopBtn.fadeIn();
            $scrollTopBtn.click(() => {
                $('body, html').animate({
                    scrollTop: 0
                }, 1000);
            });
        }
    } else {
                 $('.scroll-top-btn').remove();
          }
});
// scroll
$('a[href^="#"]').click(function () {
    $('html, body').animate({
        scrollTop: $('[name="' + $.attr(this, 'href').substr(1) + '"]').offset().top,
          }, 1000);

    return false;
});
// Close button
$("#close").click(function () {
    $(".posts").slideToggle(800);
});