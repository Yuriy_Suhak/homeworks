const imgs = document.querySelectorAll("img");
const main = document.getElementsByTagName("div");
imgs.forEach(function (item, index) {
    imgs[index].hidden = true;
});
imgs[0].hidden = false;
btnStart.disabled = true;
let timer = 0;
start();

function stop(){
    clearTimeout(timer);
    btnStop.disabled = true;
    btnStart.disabled = false;
}

function start(){
    timer = setTimeout( () => change(), 1000 );
    btnStop.disabled = false;
    btnStart.disabled = true;
}

let index = 0;
function change() {
    if(index < (imgs.length - 1)) {imgs[index].hidden = true;
        imgs[index+1].hidden = false;
        index++;
        start();
    }
    else{
        imgs[index].hidden = true;
        imgs[0].hidden = false;
        index = 0;
        start();
    }
    }

btnStop.addEventListener('click', () => { stop() });
btnStart.addEventListener('click', () => { start() });