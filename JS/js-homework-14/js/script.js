for (let i = 1; i < $("#article>li").length; ++i) {
    $("#article>li")[i].hidden = true
}
let marker = 0;

$("#navBar").on('click', 'li', function() {
    $("#navBar>li").removeClass("tabs-title-active");
    $(this).addClass("tabs-title-active");
    $("#article>li")[marker].hidden = true;
    marker = $(this).index();
    console.log(marker);
    $("#article>li")[marker].hidden = false;
});