function Hamburger()  {
    this.price = {};
    this.kcal = {};
    this.topping = {price: 0,
        kcal: 0,
        name: ""};
    this.topping2 = {price: 0,
        kcal: 0,
        name: ""};
    this.SIZE_SMALL = {price: 50,
        kcal: 20,
        name: "SIZE_SMALL",};
    this.SIZE_LARGE = {price: 100,
        kcal: 40,
        name: "SIZE_LARGE",};
    this.STUFFING_CHEESE = {price: 10,
        kcal: 20,
        name: "STUFFING_CHEESE"};
    this.STUFFING_SALAD = {price: 20,
        kcal: 5,
        name: "STUFFING_SALAD"};
    this.STUFFING_POTATO = {price: 15,
        kcal: 10,
        name: "STUFFING_POTATO"};
    this.TOPPING_MAYO = {price: 20,
        kcal: 5,
        name: "TOPPING_MAYO"};
    this.TOPPING_SPICE = {price: 15,
        kcal: 0,
        name: "TOPPING_SPICE"};

    this.MakeHamburger = function (size, stuffing) {
        this.size = size;
        this.stuffing = stuffing;
    };
    this.calculateCalories = function () {
        this.kcal = (this.size.kcal + this.stuffing.kcal + this.topping.kcal + this.topping2.kcal);
        // return this.kcal;
        console.log('kcal = ' + this.kcal);
    };
    this.calculatePrice = function () {
        this.price = (this.size.price + this.stuffing.price + this.topping.price + this.topping2.price);
        // return this.price;
        console.log('price = ' + this.price);
    };
    this.addTopping = function (topping) {
        try {
            if (!topping) {
                throw new Error("це не топінг!");

            }
            if (this.topping === topping || this.topping2 === topping)
            {throw new Error("цей топпінг вже додано")}

            if (!this.topping.price) {
                this.topping = topping;
                console.log(`'add' ${topping.name}`);
            }
            else {
                if (!this.topping2.price ) {
                    this.topping2 = topping;
                    console.log(`'add' ${topping.name}`);
                }
            }
        }
        catch (e) {
            console.log(e.message);
        }
    };
    this.removeTopping = function (topping) {
        try {
            if (!topping) {
                throw new Error("це не топінг!");
            }
            if (this.topping !== topping && this.topping2 !== topping) {
                throw new Error("цей топпінг не додавали");
            }
            if (this.topping === topping) {
                this.topping = {price: 0,
                    kcal: 0,
                    name: ""};
                console.log(`'remove' ${topping.name}`);
            }
            else {
                if (this.topping2 === topping) {
                    this.topping2 = {price: 0,
                        kcal: 0,
                        name: ""};
                    console.log(`'remove' ${topping.name}`);
                }
            }
        }
        catch (e) {
            console.log(e.message);
        }
    };
    this.getToppings = function () {
        let toppings = [this.topping, this.topping2];
        console.log(toppings);
    };
    this.getSize = function () {
        console.log(this.size.name);
    };
    this.getStuffing = function () {
        console.log(this.stuffing.name);
    };
}
let hamburger = new Hamburger();
hamburger.MakeHamburger(hamburger.SIZE_SMALL, hamburger.STUFFING_CHEESE);
hamburger.addTopping(hamburger.TOPPING_SPICE);
hamburger.addTopping(hamburger.TOPPING_SPICE);
hamburger.addTopping(hamburger.TOPPING_MAYO);
hamburger.calculateCalories();
hamburger.calculatePrice();
hamburger.removeTopping(hamburger.TOPPING_MAYO);
hamburger.calculateCalories();
hamburger.calculatePrice();
console.log(hamburger);
hamburger.addTopping(hamburger.TOPPING_CINNAMON);
hamburger.removeTopping(hamburger.TOPPING_CINNAMON);
hamburger.removeTopping(hamburger.TOPPING_MAYO);
hamburger.getToppings();
hamburger.getSize();
hamburger.getStuffing();
