class Hamburger {
    constructor(size, stuffing) {
        this.size = size;
        this.stuffing = stuffing;
        this.price = {};
        this.kcal = {};
        this.topping = {price: 0,
            kcal: 0,
            name: ""};
        this.topping2 = {price: 0,
            kcal: 0,
            name: ""};
    }
    static SIZE_SMALL = {price: 50,
        kcal: 20,
        name: "SIZE_SMALL",};
    static SIZE_LARGE = {price: 100,
        kcal: 40,
        name: "SIZE_LARGE",};
    static STUFFING_CHEESE = {price: 10,
        kcal: 20,
        name: "STUFFING_CHEESE"};
    static STUFFING_SALAD = {price: 20,
        kcal: 5,
        name: "STUFFING_SALAD"};
    static STUFFING_POTATO = {price: 15,
        kcal: 10,
        name: "STUFFING_POTATO"};
    static TOPPING_MAYO = {price: 20,
        kcal: 5,
        name: "TOPPING_MAYO"};
    static TOPPING_SPICE = {price: 15,
        kcal: 0,
        name: "TOPPING_SPICE"};
    calculateCalories = function () {
        this.kcal = (this.size.kcal + this.stuffing.kcal + this.topping.kcal + this.topping2.kcal);
        // return this.kcal;
        console.log('kcal = ' + this.kcal);
    };
    calculatePrice = function () {
        this.price = (this.size.price + this.stuffing.price + this.topping.price + this.topping2.price);
        // return this.price;
        console.log('price = ' + this.price);
    };
    addTopping = function (topping) {
        try {
            if (!topping) {
                throw new Error("це не топінг!");
            }
            if (this.topping === topping || this.topping2 === topping)
            {throw new Error("цей топпінг вже додано")}
            if (!this.topping.price) {
                this.topping = topping;
                console.log(`'add' ${topping.name}`);
            }
            else {
                if (!this.topping2.price ) {
                    this.topping2 = topping;
                    console.log(`'add' ${topping.name}`);
                }
            }
        }
        catch (e) {
            console.log(e.message);
        }
    };
   removeTopping = function (topping) {
        try {
            if (!topping) {
                throw new Error("це не топінг!");
            }
            if (this.topping !== topping && this.topping2 !== topping) {
                throw new Error("цей топпінг не додавали");
            }
            if (this.topping === topping) {
                this.topping = {price: 0,
                    kcal: 0,
                    name: ""};
                console.log(`'remove' ${topping.name}`);
            }
            else {
                if (this.topping2 === topping) {
                    this.topping2 = {price: 0,
                        kcal: 0,
                        name: ""};
                    console.log(`'remove' ${topping.name}`);
                }
            }
        }
        catch (e) {
            console.log(e.message);
        }
    };
    get toppingList() {
        return `${this.topping.name}, ${this.topping2.name}`;
    }
    get hamburgerSize() {
        return `${this.size.name}`;
    }
    get hamburgerStuffing() {
        return `${this.stuffing.name}`;
    }
};

let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(hamburger);
hamburger.calculateCalories();
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log(hamburger);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.calculateCalories();
hamburger.calculatePrice();
hamburger.removeTopping(Hamburger.TOPPING_MAYO);
hamburger.calculateCalories();
hamburger.calculatePrice();
console.log(hamburger);
hamburger.addTopping(Hamburger.TOPPING_CINNAMON);
hamburger.removeTopping(Hamburger.TOPPING_CINNAMON);
console.log(hamburger.toppingList);
console.log(hamburger.hamburgerSize);
console.log(hamburger.hamburgerStuffing);

