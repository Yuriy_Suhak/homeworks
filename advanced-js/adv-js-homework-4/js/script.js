const listCode = `<div class="card-box">
    <h4 class="title" contenteditable>Нова картка</h4>
    <ul class="card-list">
    </ul>
    <button class="add-card-button">Додати картку</button>
  </div>`;

const cardCode = `<li class="card-item" draggable="true" contenteditable></li>`;

let cards = [];
let lists = [];

addList.addEventListener('click', function () {
    let newList = document.createElement('div');
    cardCont.appendChild(newList);
    newList.outerHTML = listCode;
    lists = document.querySelectorAll(".card-box");
    [].forEach.call(lists, function(cols) {
        cols.addEventListener("drop", drop);
        cols.addEventListener("dragover", allowDrop);
    });
});

cardCont.addEventListener('click', function(e) {
    const target = e.target;
    if (target.classList == 'add-card-button') {
        let newCard = document.createElement('div');
        target.previousElementSibling.append(newCard);
        newCard.outerHTML = cardCode;
        let idCount = 1;
        cards = document.querySelectorAll(".card-item");
        [].forEach.call(cards, function(col) {
            col.addEventListener('dragstart', drag, false);
            col.id = idCount;
            idCount += 1;
        });
    }
});

function allowDrop(ev) {
    ev.preventDefault();
}
function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
    console.log(ev.dataTransfer.getData('text'));
}
function drop(ev) {
    ev.preventDefault();
    const data = ev.dataTransfer.getData("text");

    if (ev.target.classList == "card-item") {
        ev.target.before(document.getElementById(data));
    }
    else {
        if (ev.target.classList == "card-box") {
            ev.target.lastElementChild.previousElementSibling.append(document.getElementById(data));
        }
        else {
            ev.target.parentElement.lastElementChild.previousElementSibling.append(document.getElementById(data));
        }
    }
}

class column {

}